import React, { useState, useEffect } from 'react';
import Header from './components/Header';
import Drawing from './components/Drawing';
import Word from './components/Word';
import Keyboard from './components/Keyboard';
import Result from './components/Result';
import Footer from './components/Footer';

import hangman_words from './hangman_words.json';

import './App.css';

function App() {

  const [success, setSuccess] = useState(false);
  const [gameOver, setGameOver] = useState(false);
  const [word, setWord] = useState(pickRandomWord(hangman_words));
  const [pickedLetters, setPickedLetters] = useState([]);
  const [lives, setLives] = useState(6);

  useEffect(() => {
    resultCheck();
  });

  function pickRandomWord(words) {
    let randomNumber = Math.floor(Math.random() * words.length);
    return words[randomNumber].toUpperCase();
  }

  function handleButtonPress(buttonId) {
    setPickedLetters(arr => [...arr, buttonId]);
    if (!word.includes(buttonId)){
      handleMistake();
    };
  }

  function handleMistake() {
    setLives(lives - 1)
  }

  function resultCheck() {
    setSuccess(checkForWin())
    setGameOver(checkForLoss())
  }

  function checkForLoss() {
    return lives === 0;
  }

  function checkForWin() {
    let notYetPicked = word.split('').filter(letter => !pickedLetters.includes(letter));
    return !notYetPicked.length;
  }

  function getResultMessage() {
    if (gameOver) {
      return "lost";
    } else if (success) {
      return "won";
    }
    return "";
  }

  function newGame() {
    setSuccess(false);
    setGameOver(false);
    setWord(pickRandomWord(hangman_words));
    setPickedLetters([]);
    setLives(6);
  }

  return (
    <div className="main">
      <Header />
      <Drawing 
        lives = {lives}
      />
      <Word 
        word = {word}
        pickedLetters = {pickedLetters}
      />
      <Keyboard 
        handleButtonPress = {handleButtonPress}
        pickedLetters = {pickedLetters}
      />
      {(gameOver || success) && <Result 
        result = {getResultMessage()}
        handleNewGame = {newGame}
      />}
      <Footer 
        handleNewGame = {newGame}
      />
    </div>
  );
}

export default App;
