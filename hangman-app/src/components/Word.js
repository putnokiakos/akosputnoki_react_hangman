import React from 'react'
import Letter from './Letter'

const Word = ({word, pickedLetters}) => {

    function generateLetters() {
        return word.split('').map((value, index) =>
            <Letter key={'letter' + index} isVisible = {pickedLetters.includes(value.toUpperCase())} character={value} index={index}/>
        )
    }

    return (
        <div className="wordBox">
            <p className="text">It's a {word.length} letter word</p>
            <div className = "word">{generateLetters()}</div>
        </div>
    )
}

export default Word
