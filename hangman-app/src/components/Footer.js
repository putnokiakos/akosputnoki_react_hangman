import React from 'react'
import NewGameButton from './NewGameButton'

const Footer = ({handleNewGame}) => {
    return (
        <div className="footer">
            <NewGameButton handleNewGame = {handleNewGame}/>
        </div>
    )
}

export default Footer
