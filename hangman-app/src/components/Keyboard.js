import React from 'react'
import Key from './Key'

const Keyboard = ({handleButtonPress, pickedLetters}) => {

    const alphabet = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];

    return (
        <div className="keyboard">
            {alphabet.map((letter, index) => (
                <Key key={letter + index} letter={letter} handlePress={handleButtonPress} pickedLetters={pickedLetters}/>
            ))}
        </div>
    )
}

export default Keyboard
