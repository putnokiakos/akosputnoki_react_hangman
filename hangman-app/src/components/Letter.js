import React from 'react'

function Letter({isVisible, character, index}) {
    
    function handleLetterState() {
        return (isVisible === true) ? character : '';
    }
    
    return (
        <div className="letter" key={character + index}>
            {handleLetterState()}
        </div>
    )
}

export default Letter
