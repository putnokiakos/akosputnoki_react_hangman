import React from 'react'

const Key = ({letter, handlePress, pickedLetters}) => {

    function handleKeyPress () {
        handlePress(letter)
    }

    function isDisabled() {
        return pickedLetters.includes(letter);
    }
        
    return (
        <button id={letter} className="letterButton" onClick={handleKeyPress} disabled={isDisabled()}>{letter}</button>
    )
}

export default Key
