import React from 'react'

const Drawing = ({lives}) => {
    return (
        <svg height="400" width="400">
            <g id="body">
                <g>
                    <circle id="head" className={`${(lives > 5) ? "invisiblePart" : "" }`} cx="200" cy="80" r="20" stroke="black" strokeWidth="4" fill="#eed"/>
                </g>
                <line id="torso" className={`${(lives > 4) ? "invisiblePart" : "" }`} x1="200" y1="100" x2="200" y2="150" />
                <line id="armL" className={`${(lives > 3) ? "invisiblePart" : "" }`} x1="200" y1="120" x2="170" y2="140" />
                <line id="armR" className={`${(lives > 2) ? "invisiblePart" : "" }`} x1="200" y1="120" x2="230" y2="140" />
                <line id="legL" className={`${(lives > 1) ? "invisiblePart" : "" }`} x1="200" y1="150" x2="180" y2="190" />
                <line id="legR" className={`${(lives > 0) ? "invisiblePart" : "" }`} x1="200" y1="150" x2="220" y2="190" />
            </g>
            <line x1="10" y1="250" x2="150" y2="250" />
            <line id="door1" x1="150" y1="250" x2="200" y2="250" />
            <line  id="door2" x1="200" y1="250" x2="250" y2="250" />
            <line x1="250" y1="250" x2="390" y2="250" />
            <line x1="100" y1="250" x2="100" y2="20" />
            <line x1="100" y1="20" x2="200" y2="20" />
            <line id="rope" x1="200" y1="20" x2="200" y2="60" />
        </svg>
    )
}

export default Drawing
