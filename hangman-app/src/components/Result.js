import React from 'react'
import NewGameButton from './NewGameButton'

const Result = ({result, handleNewGame}) => {
    return (
        <div className="result-box">
            <div className="result">
                <p className="resultMessage">You've {result}!</p>
                <NewGameButton handleNewGame={handleNewGame}/>
            </div>
        </div>
    )
}

export default Result
