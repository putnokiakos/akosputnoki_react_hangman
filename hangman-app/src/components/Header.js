import React from 'react'

const Header = () => {
    return (
        <div className="header">
            <h1 className="text">The Hangman</h1>
        </div>
    )
}

export default Header
